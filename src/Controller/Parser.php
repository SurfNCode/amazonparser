<?php

namespace Classes;

use PHPHtmlParser\Dom;
//use Classes\Utils\CurlConnector;
use Classes\Links;

class Parser {

    private $conn;
    private $asin;
    private $link = 'https://www.amazon.de/Philips-Akkusauger-beutellos-Li-Ionen-Akku-Saugleistung/dp/B002EIJVHO/ref=olp_product_details?_encoding=UTF8&me=';
    private $dom;
    private $productDetails = [];

    private $inventoryLinks = [
//        'http://www.amazon.de/dp/B00TQQ4HRQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0713XQQ1T?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4GFE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4KAK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XS8G4D2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM56PE2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0OW8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00IO81YDM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM56PK6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4U8M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4HQC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4WZI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071ZC79JD?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071P17N29?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B004M3LH94?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0D6W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM57LMM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007BL1M7G?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4YZ1II?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6T3H8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B015YF5YNI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00L5GIP8W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XYD9HCY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DSXE01A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00XWL5YQY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVHAQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4RB2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B015YF5YEM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XYM5ZBZ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0056PQLI4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0CS6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B015YF5YBK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B015YF5SVQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B015YF5T06?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071KLDWL5?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4YZ5E8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00820HJ2W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00OA4NE4Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B015YF5SWK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B015YF5YHO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B015YF5W12?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07815HNM5?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B009WNXMQU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00820HJ9A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00CD2J16W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2VD8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0725T9KLW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTXHYU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0BFK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00XHP4LXC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0BFU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DBCIQSG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DBCIJ4C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00NGZ7GG2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVXHI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TZSVM2I?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVW0LQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00EIBAKG6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008FC6ORI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TZSVOF8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVZZS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07D1VQX1N?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TZSVPJI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00NGZ7LHG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B006GDWGK2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K7XTI2G?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0D62?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTY3HA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4MA8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01G8TV4TC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4OYJ64?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B015YF5SUC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4JOHNY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4JOMP2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTX45C?ref=myi_title_dp', //
//        'http://www.amazon.de/dp/B01FHNJX1Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00CMOWYHY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0002AEAEU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4JOHO8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0045FYE3A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00CD2CQ64?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01E5EKERA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVW2IW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00GQW9TI0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQRC3RG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01LVV4CKB?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071FJZ5VG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4KDC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT04T6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00J5ARY28?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT052W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007MFCABI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2TRG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4KBO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B076CPGY4S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4KAU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00J8KCD0I?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B004IUHLXW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072M7117K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XX9WB3H?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001TH8JJW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01CQ28NL2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTXVV4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XWWYG9J?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008DDSGO8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01HXU500E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4CGW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00DPVEUA2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01L1J0Q98?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005FQMMZU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001IJLKDI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XS7J9ZF?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0O78?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005FQMNE0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000SLQWAS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00ESX5IFC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001TH8JRY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFCKQG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTWWZ0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IT6HP2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ3TY8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IT6HRA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01N74WPIW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0I8S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IT6I4M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00ESX5668?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM56XX0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFCSU4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005FQMN9U?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0HZC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLF6OD6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM56P60?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM56XWQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVW1ME?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4JLOFI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0V1M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0I1A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFCFJI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM56PR4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0012LCQA8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06X973ZBN?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001TH8JS8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005FQMN40?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4U6E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000SLW0K4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005FW607A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06X1GDVMF?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLF59Q4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07B7W93ZL?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSH5R2D?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSH8PB9?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005FQI7IQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0725T9KL1?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00XWKX7LE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00466IXI0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005FW60DE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IT6I2O?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WD95BQJ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IT6IIS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4X0C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000SLW0ZY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XT1MQYD?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003WPGRL6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005FQI3K8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4WZS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01MYML8K0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVW1M4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0O0A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071HYTL1V?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000SLW1AS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0O1Y?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0O5K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005FWTH3Y?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WD95C92?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTWWX2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0056YP2XA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0046A7U80?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005FQMOHG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IT6I0G?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0I38?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTWWXC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVW2JG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00466IXHQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003RDLSUS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTWUSE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4HPI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072PQMD4G?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00466IXIA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06W5CD2X5?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0728F52V1?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0711C3MQN?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0HV6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07B83B966?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTWUR0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B017DG8N3E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003F2M4U4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSGPQHR?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005FW60OS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0713XPBDB?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6EGUQ8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07DNFSXCN?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVW57U?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005FWT8IS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0031OQHO0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0I56?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFCFHU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFCKMU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000SLW0YK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003NWRS5W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DR6IZF0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06X423LNW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00LOVXW8Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000SLS9C2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01GFVKGF6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071ZC7HPM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4Z2PSQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B079Y11H78?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07CQJ35C8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZE5G?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01GFVKG2Y?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4JO6CQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XD94G8F?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6FPOJQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07DN4DVJ7?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KZDMEHE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071LKKBCF?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0BDW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DVPYRSG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001AI1H26?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00008WZ0K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00MBB8102?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01G8TV82U?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001AI58TE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01LW2SOGT?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVY4K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003NWRSBQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000UUQK18?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVY3Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0728F51NS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FFFH5WU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KZDLU3I?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZDP2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005HV0H1E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071LKKBBQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IT6IYM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XCV2PRW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XD8GQN7?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071ZC7HPB?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WVM6WD6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KZDNQA8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZYH4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DC20OMQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D5F0UKG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00NHMLR0K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4OYJHI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVYQI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XDBSH63?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8ADZXW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KZDNCAM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLF6YHM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4OYJEG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XMXB8X8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01GFVKGGU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00NHWSTVA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8AE28Y?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000UUNKFC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00ANOT6C6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVW0LG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01GFVKG5Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTY2E4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000UUNKGQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000SLU9L6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXDQW2Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JU1IQE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XDDX5VH?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000UUNKES?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07172FF4J?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XD5PFMS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001TH8JJ2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZKRS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005RAKO04?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXY1LWJ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DVPYNVW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XD5PG8J?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TZSVM00?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KZDLMPY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00AHTRHBY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXCJLRH?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZDO8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D5EWWFS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTY5S2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WVHZ6S6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00T81GZ38?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XD8GRQ5?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8ADZU0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00H4NWYH8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4JOMJI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISY1KA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DVPYSBC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6FQ378?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DME3R0U?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4JO1OE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071ZC79JL?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4JO6PI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0002SMLPW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DVPYRT0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000UUNKDO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8AE29I?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DVPYOVQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07KV2NN5Z?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WGZ2WVF?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VV6PHLY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01MSVQVI5?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4JN21M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VV6PHKK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WVHZ41X?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0719MXQ84?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DVPZ2MQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXZP2JB?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00066VV56?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01G3H8B4K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4MC6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000SLW004?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WVLHM6T?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6T3WI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07LB64JSN?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLF8YV6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4JO1SU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZKQY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06W59F7QD?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQFNA0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000UUQK22?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WPBQPK5?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00008WZ1L?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K7XTAHO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4MAI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B006T30WIW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DSXDRY6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06W5VKXMG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8ADZWS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01N2QJF4E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4Z0AGA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVP2G?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8AE25M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZS16?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01E5EKEW0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0728F51ZQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01GRY5JQC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07172FF4K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WVHZ64J?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00576BT66?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8AE27U?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001AI3O3Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTY3I4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00J5ARVHG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZYGA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00ESX4I5S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071LKKBCH?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZRX0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6T3V4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4Z2JO6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01I1PA3NY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000M36AE0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00ESX5ZDM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001TH8J6K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0612?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSVMKQJ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSZ3WBX?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B004IUHC9K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2TPI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K7YPYPU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00D43DG4C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07CW6DPLL?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6ED1RY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0CPO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K7YPYRI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B002LLN7N8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6EDAWA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0DT4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FFFH6A6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KPTZ6IC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B009WNXV3E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6VU1A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00IAQDRQK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT04S2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WVLHJVN?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FVSDWJQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000CN9MXQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B076CPQ8PZ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2PHA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K7XRYYK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FFFH5MK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00538QC0G?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00D43DDKE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B002LLN7NI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B019E3NFQ4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00ESX5KR8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSHC2TG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01CSBH6AU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00576BTHU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00I6F30BC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B016ORBG3C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005X59P80?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000Z3DL9E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXRNHS2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6EG4QE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KPTZHDG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSJL3SS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XD8C3W1?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00576BLC8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00L5GG7LE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0728BPY82?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2PG6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01EAULNQ0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01HMPGH3Y?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSBTSHK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072FJ759T?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B002LLN7NS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDY3S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0002HP1LE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01CSBH3JO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B002UZIIBQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00538QALM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FFH8T64?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00AER3JAM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001TH8ISO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0716YY7HZ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4Z0FD8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01C86RW7M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2NNQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZYDS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01CSBH3SA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06X6N19WC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XCTXZ5Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007PDLXL0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FFFH6CO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01EJOY7J2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FVSDXOU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00576B8SA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2TOY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0002SMM8I?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FVSDWK0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00576BM8Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZYL0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZY6A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0BPU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWVW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K7YPYSM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KPTZFG0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01CSBH67S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01EJOY74W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00ESX5KL4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01C86RWJK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06W59KDWS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B019E3NFVO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06X6MX9TP?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001AI3OF4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6ED81S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2NMC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K7XS8P4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00J5ARVI0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2QXI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FFFH6RE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WVLHL2F?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XTSTWL2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4Z2JVE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06W5CBCBV?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00LL5SR6C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZY3I?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01G8TV4XS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2PL6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4Z2K2W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06VYBMV5V?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZEGA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00FLT5GXG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00J5ARVL2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003O8NWQY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00576BT20?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00FL0AQD0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001TH8ISE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001AI728E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00538QCB0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4OYJK0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FVSDW82?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XY35NX7?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2PFC?ref=myi_title_dp',
        'http://www.amazon.de/dp/B01J86YNN6?ref=myi_title_dp',
        'http://www.amazon.de/dp/B07LB79BTR?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00K4Z2KD6?ref=myi_title_dp',
        'http://www.amazon.de/dp/B0711SLB3K?ref=myi_title_dp',
        'http://www.amazon.de/dp/B06WVHZ6KJ?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00TQQ2PH0?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00K7YQ2JM?ref=myi_title_dp',
        'http://www.amazon.de/dp/B06XSSLFGK?ref=myi_title_dp',
        'http://www.amazon.de/dp/B010PRZSO0?ref=myi_title_dp',
        'http://www.amazon.de/dp/B01G8TV4WO?ref=myi_title_dp',
        'http://www.amazon.de/dp/B0093X928G?ref=myi_title_dp',
        'http://www.amazon.de/dp/B003IX0CWW?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00K7XSDP4?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00ESX4QT6?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00008WZ0Z?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00ESX5384?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00K7XSEE4?ref=myi_title_dp',
        'http://www.amazon.de/dp/B01H68Z11Q?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00J2KBKO4?ref=myi_title_dp',
        'http://www.amazon.de/dp/B003SLDXXO?ref=myi_title_dp',
        'http://www.amazon.de/dp/B01HMPGFM2?ref=myi_title_dp',
        'http://www.amazon.de/dp/B01N4AK905?ref=myi_title_dp',
        'http://www.amazon.de/dp/B003IX0CYK?ref=myi_title_dp',
        'http://www.amazon.de/dp/B06XYMWP8F?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00XHP4YIO?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00BQTX556?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00KLFACQG?ref=myi_title_dp',
        'http://www.amazon.de/dp/B07179YZPG?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00EPDQ5HU?ref=myi_title_dp',
        'http://www.amazon.de/dp/B001B6QXNK?ref=myi_title_dp',
        'http://www.amazon.de/dp/B001PHBUQ0?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00KLFACKM?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00TQQ2WM8?ref=myi_title_dp',
        'http://www.amazon.de/dp/B003IX0CXG?ref=myi_title_dp',
        'http://www.amazon.de/dp/B001B6T472?ref=myi_title_dp',
        'http://www.amazon.de/dp/B003SLDWL2?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00820HM3I?ref=myi_title_dp',
        'http://www.amazon.de/dp/B007BKZ1T2?ref=myi_title_dp',
        'http://www.amazon.de/dp/B06XT3BZRV?ref=myi_title_dp',
        'http://www.amazon.de/dp/B071SBLLB7?ref=myi_title_dp',
        'http://www.amazon.de/dp/B0714Q9Z9C?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00EPDQ6VA?ref=myi_title_dp',
        'http://www.amazon.de/dp/B001B6VTOS?ref=myi_title_dp',
        'http://www.amazon.de/dp/B01DSXDVYM?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00TQQ2Y9E?ref=myi_title_dp',
        'http://www.amazon.de/dp/B06XWZ6RD9?ref=myi_title_dp',
        'http://www.amazon.de/dp/B01MXUUGJ8?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00ESX64F0?ref=myi_title_dp',
        'http://www.amazon.de/dp/B003IX0BQO?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00A91TJ9S?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00008WZ1E?ref=myi_title_dp',
        'http://www.amazon.de/dp/B06XXCJLRF?ref=myi_title_dp',
        'http://www.amazon.de/dp/B000M37VUW?ref=myi_title_dp',
        'http://www.amazon.de/dp/B000M37VB6?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00KLFARMU?ref=myi_title_dp',
        'http://www.amazon.de/dp/B001B6VRXG?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00K6OB6I0?ref=myi_title_dp',
        'http://www.amazon.de/dp/B071VCCH1Z?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00XHP4MCM?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00BQTX600?ref=myi_title_dp',
        'http://www.amazon.de/dp/B003IX0BQE?ref=myi_title_dp',
        'http://www.amazon.de/dp/B001B6VS30?ref=myi_title_dp',
        'http://www.amazon.de/dp/B01DSXDS4K?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00KLF6NTG?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00XWK3GSS?ref=myi_title_dp',
        'http://www.amazon.de/dp/B000VJA2Y4?ref=myi_title_dp',
        'http://www.amazon.de/dp/B01EJOYNHS?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00TQQ4CIU?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00K7YPV8A?ref=myi_title_dp',
        'http://www.amazon.de/dp/B008MVVIZU?ref=myi_title_dp',
        'http://www.amazon.de/dp/B0711W1K2H?ref=myi_title_dp',
        'http://www.amazon.de/dp/B01DISZRQW?ref=myi_title_dp',
        'http://www.amazon.de/dp/B003IX0CZ4?ref=myi_title_dp',
        'http://www.amazon.de/dp/B01EJOYNSC?ref=myi_title_dp',
        'http://www.amazon.de/dp/B003IX0B46?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00E8ADRYO?ref=myi_title_dp',
        'http://www.amazon.de/dp/B000XJA1O8?ref=myi_title_dp',
        'http://www.amazon.de/dp/B0034KYPCW?ref=myi_title_dp',
        'http://www.amazon.de/dp/B06XXZN3HX?ref=myi_title_dp',
        'http://www.amazon.de/dp/B000V7C0C8?ref=myi_title_dp',
        'http://www.amazon.de/dp/B06X9TGVWG?ref=myi_title_dp',
        'http://www.amazon.de/dp/B003IX0D5I?ref=myi_title_dp',
        'http://www.amazon.de/dp/B000CNBEQE?ref=myi_title_dp',
        'http://www.amazon.de/dp/B007FDVI4M?ref=myi_title_dp',
        'http://www.amazon.de/dp/B003IX0CQ8?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00KLF9T74?ref=myi_title_dp',
        'http://www.amazon.de/dp/B000M38034?ref=myi_title_dp',
        'http://www.amazon.de/dp/B003IX0D6C?ref=myi_title_dp',
        'http://www.amazon.de/dp/B01HXU4VTU?ref=myi_title_dp',
        'http://www.amazon.de/dp/B00008WZ1D?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007ASSQDS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072J8G1KG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DSXDS3Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072F2GYWJ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFAUY0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4Z2JMI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXYG6CT?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQFKOE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2WPA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00EPDQ3TA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0002AEAEK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00JBEX6QQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4DQ6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0BQY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6OZXU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00XHP4MV8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01HXU4VAY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KBRS6DU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071VZ6GMH?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XY6VQ9Z?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000SLU9Q6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DSXDW00?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFARMK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071P55YQY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00J8KC31M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00H4NTNEA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTX420?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DSXE042?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000XJC15K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQFNO6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008FR48MG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000SLS92W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0D0I?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM579V0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00N4XP2VW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06X1GDPQF?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XYDCZW4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6P0IY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2WT6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DSXDW1Y?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWRG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6P0K2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00008WZ1A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6T45O?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0CVI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQFH1U?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071VCCJDJ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00820HLS4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0171A81SE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTX5ZQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007BKY256?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM57LIG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0CYA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01EJOZ7IM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01LZ8ZVAY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07258V218?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07K8V24M5?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8ADLAY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00A0PXHCI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B077H1V38L?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DVQ0130?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07HNC1V7L?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07BM1VRCM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZEA6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07B5H7GZD?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6QXTO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07C8SC2KM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWR6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFAN7E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07B5BHK57?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07C5CPBFV?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0D08?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01CUBBD0C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07BB9D9D5?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XD441Q7?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6O5TM4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0017J6XG8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B078K2VQZG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07CCDQRGH?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000M3AZSW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07BN2CVPQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003RDN5D6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07C58MDN6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFAX7E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0725T9CY5?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07G3FFX4V?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07BMY7ZBN?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07C5BGGN5?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DSXDW32?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0CW2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07BMY6L22?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07172FFVT?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07C58M947?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0015LKIYG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00XHP4RH2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07C8J9N3X?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07C7DPX6G?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XQK443K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B009WNXR60?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07C5BG8GQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WPBQT7N?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07FP1X869?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XYH2D28?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0725T9KLS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B002LLN7RY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTY3MK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005OJX8F6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06VYBMSKF?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8ADXES?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DVQ02PW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVW4DU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07FPB8HMZ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07CTMF5CF?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZ7W6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07172FFCQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DSXDW82?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B002HEI4LO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07B57PRZ5?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXZQ4Y2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0034KYPAY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0719WKLSK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0CX6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTXPMY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DSXDRVO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003RDN7G6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFB04Y?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLF59NW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFA928?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07237JNK8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTX56A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6T4TU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0711W1KN2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQFK62?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01EJOZ7KA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FVSDWM8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWQW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0725T9CXY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07172FF3N?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072PQMD4S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071LKKBBF?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B077VW8RXT?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01AC0YJT0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072PR2XKJ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01EJOZ7YQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B002LLN7RO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFAD4C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072PQMD5V?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQFP12?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01CQ1ZO56?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000V7HFC8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWLW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00XWL0CDY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0DRG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4Z0GFK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTX55Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01LXHFW3D?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DSXDVXI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0CUY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFB0E4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FARHPY6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0BDM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071L5XQMF?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVIXM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01GFQUJCQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4DRK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLF9OLK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQFP0S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0D6M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000M3674S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000M3AY7E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01EAUYNOO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTY4NI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00820I124?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM57LE0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00ESX4TBG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XYM5Y14?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B006P0DQ8C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTX614?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01AC0WV1S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001AI1H9O?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072C8KDZN?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0711RDF7S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B010PRZNZY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072BZXYCM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFACQ6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTX60A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XYM4S2P?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTX538?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00XWK3KDO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM57LBI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000M368L0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6OZM6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQEV0I?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000M3658Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQFOQI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZEM4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000M369SM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06W5VR557?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6VR8Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDY10?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0BIC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQFKOO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0CXQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01HXU4UZK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWYO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B077VZX3WS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWLM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XYNPCV8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4DUC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B002LLN7SS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071LFFP1M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0716HWCDK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01HXU4V8Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXJ2M1J?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0093X91ZU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0BEG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00EPDQ3T0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06W5LTKWH?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQEUQ8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06X6JBKKV?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQFMZ6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XJN1FP9?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XT3BLJY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISXM7I?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IT6I6A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2WPK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4SS4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0C8G?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0711B994X?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISYOY8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXDPQWF?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071ZC7H2R?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8ADS4I?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVKF8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXC61FG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZ7P8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XWY48Y2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQEUYU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSHK2JM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSJNTZS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6P2TQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001TH8JGU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072HMMKQ8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DVPYSIK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFC54I?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00XHP4XPI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXDM93V?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6EG11W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06VWGMGT8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6VS7Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2YCG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XJZWPJK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WD5GLXB?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXGRLKW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8ADXCK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0C04?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWNA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XWT2B1L?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ465Y?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDX3Y?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSHK2JH?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXCGJ5V?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000M366SA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTXDBW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00008WZ1F?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07237JFT7?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DSXE04C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XX91FJF?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQEUPE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZ1MW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WRWZWK4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000M39J48?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6EFSWK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISYO4S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVIZ0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FARHP00?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07C5BK7SY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07172FFW9?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07CFY5RNG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6T4WC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0DGM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTXPLU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000XJE3O2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06W5C98H4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQFMPG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DVPZ3RA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01EJOYV02?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KZDLTZW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISZ7RG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DIT0OAU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0DIK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001TH8JJM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0C8Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XS1BZ3J?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6ECT5E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06X6JBMQP?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQEUPY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ49MO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005HGYEVI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVH7O?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4X3Y?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WLMRX9M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001QK1S3G?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6ECT3Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2WSW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B002NFQ1LC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0DVC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSSJQNQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6QX1M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4YZE8A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2YEO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ462W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XT2MJLT?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WGYNH59?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0DF8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISYOIO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0DI0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTXPME?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFAN4C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WLMS1ZT?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BN6AA50?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFBXZK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071ZC79JV?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWZ8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00ESX4T5C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSHMS5P?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0DH6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4DR0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISYAXS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01CQ24SF2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0DHG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072F21DBX?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISYI2G?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTX60U?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ49O2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ3GP0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FHNJW58?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6EGFOA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6T6K2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0C0O?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVIX2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6EFPLE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8ADOY2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXDBMBN?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWRQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSYJTY9?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0DGW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXC7BMK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISXFWU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0DEY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXD36LQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISYB5U?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00008WZ1G?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XSKDBHL?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WGRG9X9?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2WTQ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ3VKK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XX9424V?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXJ2M1J?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B007JQFNO6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B002LLN7N8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4Z2K2W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6FQ378?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06X9Q2PX9?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00008WZ1H?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0048BOXIW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06X95R2GG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01CSBH682?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISYW2M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXZQ4Y2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XY24J6M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXJ2L9C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXZJ2V7?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D5EI2L6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07C5BK7SY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISYOE8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6EDAWA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K6EG4QE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFAN7E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFB04Y?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTX42U?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWVW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000UUNKDO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0002SMLPW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01LW2SOGT?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0C3Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01C86RWJK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000XUPGL0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00576BSN0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4Z2PJK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTY4NI?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0015LKIYG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001AI3O3Q?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B002LLN7SS?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K4Z2JVE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WVM6YMX?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00576BSN0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001FDERS2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B010PRZNZY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07DX84L54?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XXYG6CT?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01CPHPQ2M?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07B5GDC8S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B0711SLB3K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM579V0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTY2E4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000SLU9Q6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWLW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWL2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0C4A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001PHBUQ0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00ESX5KL4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01C86RWZE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071VZ6GMH?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00XHP4RH2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00XHP4MCM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFA928?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLFARMU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000SLU9L6?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000UUQK22?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DVQ0130?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00AHTRHBY?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B005HV0H6E?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06WVLHL2F?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07B57FJZP?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B071VCCJDJ?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07237JNPW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XK3YZWL?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ2YEO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTX614?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000M3674S?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00008WZ0K?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDY10?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001AI1H26?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B06XT3BZRV?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072352ZSR?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B008MVVIX2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWQW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWQW?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B000XJE3O2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003RDLOSO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DSXE01A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00XWKX7LE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00VM57LE0?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01CPHPMQM?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07B6CJCK9?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01FVSDXOU?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDXXO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001B6VSMG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01DISY1LO?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01D4OYJ32?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01E5EKERA?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8AE29I?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00TQQ4MA8?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8ADXCK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01EAUYK60?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B072C8KDZN?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B07B4KXQY4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KLF5DII?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B004T8DAWE?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003IX0CYK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00E8ADXES?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00BQTY3MK?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B01CUBBD0C?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B015YF5SUC?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDZEG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003NWRS5W?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWRG?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00K7XSEE4?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B00KZDM0D2?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B001AI1H3A?ref=myi_title_dp',
//        'http://www.amazon.de/dp/B003SLDWQW?ref=myi_title_dp'
    ];

    private $isDebug = true;

    public function __construct() {
//        $test = 'http://www.amazon.de/dp/B00TQQ4HRQ?ref=myi_title_dp';
//        echo file_get_contents($test);
//        $this->dom->loadFromUrl($test);
//        $items = $this->dom->find('.a-color-price');
//        echo "count: ".count($items);

        $this->conn = $this->mysqlConnect();
        $this->dom = new Dom;
        foreach($this->inventoryLinks as $key => $link) {
            if($key % rand(6,17) == 0) {
                $slep = $this->getRandSleepTime(3,8);
                echo "sleeping for: ".$slep."<br>";
                sleep($slep);
            }
            $this->setLink($link);
            $this->setAsin($this->parseAsin($this->getLink()));
            $this->getNewAndUsedProductDetails($key);
            die();
        }
    }

    private function mysqlConnect() {

        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dbname = "wenko";

        if(!$this->isDebug) {
            $servername = "auth-db175.hostinger.com";
            $username = "u506259361_root";
            $password = "rei`5eISyI2EedH";
            $dbname = "u506259361_wenko";
        }

        // Create connection
        $conn = new \mysqli($servername, $username, $password, $dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        return $conn;
    }

    private function getProductDetailLink() {
//        $this->dom->loadFromUrl($produktlink);
//        //Get used stuff link
//        $plinks = $this->dom->find('.olp-padding-right');
//
//        foreach ($plinks as $key => $linkItem) {
//            $finds = '';
//            preg_match('/(new|used)/', $plinks[0]->firstChild()->getAttribute('href'), $finds);
//            $productDetailLink[$finds[0]] = "https://www.amazon.de".str_replace('amp;', '', $plinks[0]->firstChild()->getAttribute('href'));
//        }
//        //https://www.amazon.de/gp/offer-listing/B00TQQ4HRQ/ref=dp_olp_new?ie=UTF8&condition=new
//        //https://www.amazon.de/gp/offer-listing/B00TQQ4HRQ/ref=olp_f_used?ie=UTF8&f_new=true&f_usedAcceptable=true&f_usedGood=true&f_used=true&f_usedLikeNew=true&f_usedVeryGood=true
//        if(isset($productDetailLink['new'])) {
//            $link = $productDetailLink['new'];
//        } elseif(isset($productDetailLink['used'])) {
//            $link = $productDetailLink['used'];
//        }
        $link = 'http://www.amazon.de/gp/offer-listing/'.$this->getAsin().'/ref=olp_f_used?ie=UTF8&f_new=true&f_usedAcceptable=true&f_usedGood=true&f_used=true&f_usedLikeNew=true&f_usedVeryGood=true';
        echo $link;
        return $link;
    }

    private function getNewAndUsedProductDetails($key) {
        $link = $this->getProductDetailLink();
//        echo "link: ".$link." <br>";

        //Get links to pages of used products
        $this->dom->loadFromUrl($link);
//        $this->dom->loadFromFile('adetail.html');

        sleep($this->getRandSleepTime(2,5));
        $pageInfo = $this->dom->find('.a-pagination');
        if(count($pageInfo) == 0) {
            $pages = 0;
            $newAndUsedLinks[0] = $link;
        } else {
            $noPages = $pageInfo->find('li');
            $pages = count($noPages)-2;

            $newAndUsedLinks = [];
            $baseUrl = substr($link, 0, strpos($link, 'ref'));
            for($i = 1; $i<=$pages; $i++) {
                $pIndex = ($i-1)*10;
                $linkParams = 'ref=olp_page_'.$i.'?ie=UTF8&f_all=true&f_new=true&f_used=true&f_usedAcceptable=true&f_usedGood=true&f_usedLikeNew=true&f_usedVeryGood=true&startIndex='.$pIndex;
                $newAndUsedLinks[$i] = $baseUrl.$linkParams;
            }
        }
//        print_r($newAndUsedLinks);
        $productList = [];
        foreach($newAndUsedLinks as $key => $l) {
            if($key == 0) {
                $loadPageAgain = 0;
            } else {
                $loadPageAgain = 1;
            }
            $p = $this->findProductDetails($l, $loadPageAgain);
            array_push($productList, $p);
        }
        $this->addProductsToDb($productList);
//        print_r($productList);
        $ext = $this->findExtremes($productList);
        $this->addExtremesToDb($ext);
    }

    private function parseAsin($link) {
        $finds = '';
        preg_match('/[A-Z0-9]{10,}/', $link, $finds);
        return $finds[0];
    }

    function findProductDetails($link, $loadPageAgain) {
        if($loadPageAgain) {
            $this->dom->loadFromUrl($link);
            sleep($this->getRandSleepTime(2,5));
        }

        $usedBlocks = $this->dom->find('.olpOffer');
        echo "Found: ".count($usedBlocks)." products</br>";
        $productDetails = [];
        //only continue if there are actually products available to parse
        if(count($usedBlocks)) {
            foreach($usedBlocks as $key => $usedBlock) {
                $productDetails[$key]['shipping'] = 0;
                if(count($usedBlock->find('.olpShippingPrice')) > 0) {
                    $productDetails[$key]['shipping'] = $this->toNumber($this->replaceEur($usedBlock->find('.olpShippingPrice')->innerHtml()));
                }
                $productDetails[$key]['price'] = $this->toNumber($this->replaceEur($usedBlock->find('.olpOfferPrice')->innerHtml()));
                $productDetails[$key]['condition'] = rtrim(ltrim($usedBlock->find('.olpCondition')->innerHtml()));
            }
        }
        return $productDetails;
    }

    private function findExtremes($arr) {
        $product = [];
        foreach ($arr as $key1 => $item) {
            foreach ($item as $key => $prod) {
                $condition = $prod['condition'];
                $price = $prod['price'] + $prod['shipping'];

                if(isset($product['cheapest'][$condition])) {
                    if($price < $product['cheapest'][$condition]) {
                        $product['cheapest'][$condition] = $price;
                    }
                } else {
                    $product['cheapest'][$condition] = $price;
                }

                if(isset($product['cheapestOverall'])) {
                    if($price < $product['cheapestOverall']) {
                        $product['cheapestOverall'] = $price;
                    }
                } else {
                    $product['cheapestOverall'] = $price;
                }

                if(isset($product['expensive'][$condition])) {
                    if($price > $product['expensive'][$condition]) {
                        $product['expensive'][$condition] = $price;
                    }
                } else {
                    $product['expensive'][$condition] = $price;
                }
            }
        }
        return $product;
    }

    private function replaceEur($str) {
        $finds = ['/[\s]+/', '/EUR/ '];
        $replace = ['', ''];
        return preg_replace($finds, $replace, $str);
    }

    private function toNumber($str) {
        return floatval(str_replace(',', '.', $str));
    }

    private function saveAsinToDb($asin) {
        $sql = "INSERT INTO productDetails (asin) VALUES ('{$asin}')";
        if ($this->conn->query($sql) === TRUE) {

        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }

    private function addProductsToDb($products) {
        foreach ($products as $key1 => $item) {
            foreach ($item as $key => $prod) {
                $sql = "INSERT INTO products (asin, shippingCost, price, cond) VALUES ('{$this->asin}', '{$prod['shipping']}', '{$prod['price']}', '{$prod['condition']}')";
                if ($this->conn->query($sql) === TRUE) {

                } else {
                    echo "Error: " . $sql . "<br>" . $this->conn->error;
                }
            }
        }
    }

    private function addExtremesToDb($ext) {
//        foreach($ext['cheapest'] as $cond => $price) {
        $gSehrGut = (isset($ext['cheapest']['Gebraucht - Sehr gut'])) ? $ext['cheapest']['Gebraucht - Sehr gut'] : "null";
        $gGut = (isset($ext['cheapest']['Gebraucht - Gut'])) ? $ext['cheapest']['Gebraucht - Gut'] : "null";
        $gAkz = (isset($ext['cheapest']['Gebraucht - Akzeptabel'])) ? $ext['cheapest']['Gebraucht - Akzeptabel'] : "null";
        $gWieNeu = (isset($ext['cheapest']['Gebraucht - Wie neu'])) ? $ext['cheapest']['Gebraucht - Wie neu'] : "null";
        $neu = (isset($ext['cheapest']['Neu'])) ? $ext['cheapest']['Neu'] : "null";
        $low = (isset($ext['cheapestOverall'])) ? $ext['cheapestOverall'] : "null";

        $sql = "INSERT INTO priceExtremes (asin, low,  GebrauchtSehrGut, GebrauchtGut, GebrauchtAkzeptabel, GebrauchtWieNeu, Neu)
          VALUES ('{$this->getAsin()}', {$low}, {$gSehrGut}, {$gGut}, {$gAkz}, {$gWieNeu}, {$neu})";
        if ($this->conn->query($sql) === TRUE) {
            echo "ASIN: ".$this->getAsin()." added to db</br>";
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }

    /**
     * @return mixed
     */
    public function getAsin()
    {
        return $this->asin;
    }

    /**
     * @param mixed $asin
     */
    public function setAsin($asin)
    {
        echo "ASIN: ".$asin."</br>";
        $this->asin = $asin;
    }

    /**
     * @return mixed|string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed|string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getRandSleepTime($min, $max)
    {
        return rand($min, $max);
    }
}
