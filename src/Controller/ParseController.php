<?php

namespace App\Controller;

use App\Utils\Parser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ParseController extends AbstractController
{
    /**
     * @Route("/parse", name="parse")
     */
    public function index(Parser $parser)
    {
        $return = $parser->getNewAndUsedProductDetails(null);
        dump($return);
        die();
        return new Response($return);
    }
}
