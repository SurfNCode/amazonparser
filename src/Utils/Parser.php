<?php
/**
 * Created by PhpStorm.
 * User: n.dilthey
 * Date: 2018-12-19
 * Time: 15:26
 */

namespace App\Utils;

use PHPHtmlParser\Dom;

class Parser
{
    private $dom;
    private $curl;
    const VIDEO = 'video';
    const ZIP = 'zip';
    const PDF = 'pdf';

    public function __construct(Curl $curl) {
        $this->conn = $this->mysqlConnect();
        $this->dom = new Dom;
        foreach($this->inventoryLinks as $key => $link) {
            if($key % rand(6,17) == 0) {
                $slep = $this->getRandSleepTime(3,8);
                echo "sleeping for: ".$slep."<br>";
                sleep($slep);
            }
            $this->setLink($link);
            $this->setAsin($this->parseAsin($this->getLink()));
            $this->getNewAndUsedProductDetails($key);
            die();
        }
    }

    private function mysqlConnect() {

        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dbname = "wenko";

        if(!$this->isDebug) {
            $servername = "auth-db175.hostinger.com";
            $username = "u506259361_root";
            $password = "rei`5eISyI2EedH";
            $dbname = "u506259361_wenko";
        }

        // Create connection
        $conn = new \mysqli($servername, $username, $password, $dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        return $conn;
    }

    private function getProductDetailLink() {
        $link = 'http://www.amazon.de/gp/offer-listing/'.$this->getAsin().'/ref=olp_f_used?ie=UTF8&f_new=true&f_usedAcceptable=true&f_usedGood=true&f_used=true&f_usedLikeNew=true&f_usedVeryGood=true';
        echo $link;
        return $link;
    }

    private function getNewAndUsedProductDetails($key) {
        $link = $this->getProductDetailLink();
//        echo "link: ".$link." <br>";

        //Get links to pages of used products
        $this->dom->loadFromUrl($link);
//        $this->dom->loadFromFile('adetail.html');

        sleep($this->getRandSleepTime(2,5));
        $pageInfo = $this->dom->find('.a-pagination');
        if(count($pageInfo) == 0) {
            $pages = 0;
            $newAndUsedLinks[0] = $link;
        } else {
            $noPages = $pageInfo->find('li');
            $pages = count($noPages)-2;

            $newAndUsedLinks = [];
            $baseUrl = substr($link, 0, strpos($link, 'ref'));
            for($i = 1; $i<=$pages; $i++) {
                $pIndex = ($i-1)*10;
                $linkParams = 'ref=olp_page_'.$i.'?ie=UTF8&f_all=true&f_new=true&f_used=true&f_usedAcceptable=true&f_usedGood=true&f_usedLikeNew=true&f_usedVeryGood=true&startIndex='.$pIndex;
                $newAndUsedLinks[$i] = $baseUrl.$linkParams;
            }
        }
//        print_r($newAndUsedLinks);
        $productList = [];
        foreach($newAndUsedLinks as $key => $l) {
            if($key == 0) {
                $loadPageAgain = 0;
            } else {
                $loadPageAgain = 1;
            }
            $p = $this->findProductDetails($l, $loadPageAgain);
            array_push($productList, $p);
        }
        $this->addProductsToDb($productList);
//        print_r($productList);
        $ext = $this->findExtremes($productList);
        $this->addExtremesToDb($ext);
    }

    private function parseAsin($link) {
        $finds = '';
        preg_match('/[A-Z0-9]{10,}/', $link, $finds);
        return $finds[0];
    }

    function findProductDetails($link, $loadPageAgain) {
        if($loadPageAgain) {
            $this->dom->loadFromUrl($link);
            sleep($this->getRandSleepTime(2,5));
        }

        $usedBlocks = $this->dom->find('.olpOffer');
        echo "Found: ".count($usedBlocks)." products</br>";
        $productDetails = [];
        //only continue if there are actually products available to parse
        if(count($usedBlocks)) {
            foreach($usedBlocks as $key => $usedBlock) {
                $productDetails[$key]['shipping'] = 0;
                if(count($usedBlock->find('.olpShippingPrice')) > 0) {
                    $productDetails[$key]['shipping'] = $this->toNumber($this->replaceEur($usedBlock->find('.olpShippingPrice')->innerHtml()));
                }
                $productDetails[$key]['price'] = $this->toNumber($this->replaceEur($usedBlock->find('.olpOfferPrice')->innerHtml()));
                $productDetails[$key]['condition'] = rtrim(ltrim($usedBlock->find('.olpCondition')->innerHtml()));
            }
        }
        return $productDetails;
    }

    private function findExtremes($arr) {
        $product = [];
        foreach ($arr as $key1 => $item) {
            foreach ($item as $key => $prod) {
                $condition = $prod['condition'];
                $price = $prod['price'] + $prod['shipping'];

                if(isset($product['cheapest'][$condition])) {
                    if($price < $product['cheapest'][$condition]) {
                        $product['cheapest'][$condition] = $price;
                    }
                } else {
                    $product['cheapest'][$condition] = $price;
                }

                if(isset($product['cheapestOverall'])) {
                    if($price < $product['cheapestOverall']) {
                        $product['cheapestOverall'] = $price;
                    }
                } else {
                    $product['cheapestOverall'] = $price;
                }

                if(isset($product['expensive'][$condition])) {
                    if($price > $product['expensive'][$condition]) {
                        $product['expensive'][$condition] = $price;
                    }
                } else {
                    $product['expensive'][$condition] = $price;
                }
            }
        }
        return $product;
    }

    private function replaceEur($str) {
        $finds = ['/[\s]+/', '/EUR/ '];
        $replace = ['', ''];
        return preg_replace($finds, $replace, $str);
    }

    private function toNumber($str) {
        return floatval(str_replace(',', '.', $str));
    }

    private function saveAsinToDb($asin) {
        $sql = "INSERT INTO productDetails (asin) VALUES ('{$asin}')";
        if ($this->conn->query($sql) === TRUE) {

        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }

    private function addProductsToDb($products) {
        foreach ($products as $key1 => $item) {
            foreach ($item as $key => $prod) {
                $sql = "INSERT INTO products (asin, shippingCost, price, cond) VALUES ('{$this->asin}', '{$prod['shipping']}', '{$prod['price']}', '{$prod['condition']}')";
                if ($this->conn->query($sql) === TRUE) {

                } else {
                    echo "Error: " . $sql . "<br>" . $this->conn->error;
                }
            }
        }
    }

    private function addExtremesToDb($ext) {
//        foreach($ext['cheapest'] as $cond => $price) {
        $gSehrGut = (isset($ext['cheapest']['Gebraucht - Sehr gut'])) ? $ext['cheapest']['Gebraucht - Sehr gut'] : "null";
        $gGut = (isset($ext['cheapest']['Gebraucht - Gut'])) ? $ext['cheapest']['Gebraucht - Gut'] : "null";
        $gAkz = (isset($ext['cheapest']['Gebraucht - Akzeptabel'])) ? $ext['cheapest']['Gebraucht - Akzeptabel'] : "null";
        $gWieNeu = (isset($ext['cheapest']['Gebraucht - Wie neu'])) ? $ext['cheapest']['Gebraucht - Wie neu'] : "null";
        $neu = (isset($ext['cheapest']['Neu'])) ? $ext['cheapest']['Neu'] : "null";
        $low = (isset($ext['cheapestOverall'])) ? $ext['cheapestOverall'] : "null";

        $sql = "INSERT INTO priceExtremes (asin, low,  GebrauchtSehrGut, GebrauchtGut, GebrauchtAkzeptabel, GebrauchtWieNeu, Neu)
          VALUES ('{$this->getAsin()}', {$low}, {$gSehrGut}, {$gGut}, {$gAkz}, {$gWieNeu}, {$neu})";
        if ($this->conn->query($sql) === TRUE) {
            echo "ASIN: ".$this->getAsin()." added to db</br>";
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
    }

    /**
     * @return mixed
     */
    public function getAsin()
    {
        return $this->asin;
    }

    /**
     * @param mixed $asin
     */
    public function setAsin($asin)
    {
        echo "ASIN: ".$asin."</br>";
        $this->asin = $asin;
    }

    /**
     * @return mixed|string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed|string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getRandSleepTime($min, $max)
    {
        return rand($min, $max);
    }
}